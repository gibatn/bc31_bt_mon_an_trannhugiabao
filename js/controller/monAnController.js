export let monAnController = {
  layThongTinTuForm: () => {
    let tenMon = document.getElementById("txt-ten").value;
    let gia = document.getElementById("txt-gia").value;
    let moTa = document.getElementById("txt-mo-ta").value;
    let monAn = {
      name: tenMon,
      price: gia,
      description: moTa,
    };
    return monAn;
  },
  showThongTinLenForm: (res) => {
    document.getElementById("txt-ten").value = res.data.name;
    document.getElementById("txt-gia").value = res.data.price;
    document.getElementById("txt-mo-ta").value = res.data.description;
  },
};
