// https://62b07874e460b79df0469ac8.mockapi.io/
import { monAnController } from "./controller/monAnController.js";
import { monAnService } from "./service/monAnService.js";
import { spinnerService } from "./service/spinnerService.js";
// import{spinnerService} from "./service/spinnerService.js";

let foodList = [];
let idFooddEdited = null;
// function xóa món ăn
let xoaMonAn = (maMonAn) => {
  monAnService
    .xoaMonAn(maMonAn)
    .then((res) => {
      console.log(res);
      renderDanhSachService();
    })
    .catch((err) => {
      console.log(err);
    });
};

window.xoaMonAn = xoaMonAn;

let renderTable = (list) => {
  let contentHTML = "";
  for (let index = 0; index < list.length; index++) {
    let monAn = list[index];
    let contentTr = `<tr> 
                         <td> ${monAn.id} </td>
                         <td> ${monAn.name} </td>
                         <td> ${monAn.price} </td>
                         <td> ${monAn.description} </td>
                         <td>
                         <button class ="btn btn-success" onclick='layChiTietMonAn(${monAn.id})'>Sửa</button>
                         <button class ="btn btn-danger" onclick='xoaMonAn(${monAn.id})'>Xóa</button>
                         </td>
                     </tr>`;
    contentHTML = contentHTML + contentTr;
  }
  console.log("contentHTML", contentHTML);
  document.getElementById("tbody_food").innerHTML = contentHTML;
  // render danh sách món ăn ra ngoài màn hình
};

let renderDanhSachService = () => {
  spinnerService.batLoading();
  monAnService
    .layDanhSachMonAn()
    .then((res) => {
      // console.log(res.data);
      foodList = res.data;
      console.log(foodList);
      spinnerService.tatLoading();

      renderTable(foodList);
    })
    .catch((err) => {
      spinnerService.tatLoading();
      console.log(err);
    });
};
// chạy lần đầu khi load lại trang
renderDanhSachService();

let themMonAn = () => {
  let monAn = monAnController.layThongTinTuForm();
  spinnerService.batLoading();
  monAnService
    .themMoiMonAn(monAn)
    .then((res) => {
      // alert("thành công");
      spinnerService.tatLoading();
      renderDanhSachService();
    })
    .catch((err) => {
      spinnerService.tatLoading();

      alert("thất bại");
    });
};
window.themMonAn = themMonAn;

let layChiTietMonAn = (idMonAn) => {
  idFooddEdited = idMonAn;
  spinnerService.batLoading();
  monAnService
    .layThongTinChiTietMonAn(idMonAn)
    .then((res) => {
      monAnController.showThongTinLenForm(res);
      spinnerService.tatLoading();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.layChiTietMonAn = layChiTietMonAn;

let capNhatMonAn = () => {
  spinnerService.batLoading();
  let monAn = monAnController.layThongTinTuForm();
  console.log(monAn);

  let newMonAn = { ...monAn, id: idFooddEdited };

  monAnService
    .capNhatMonAn(newMonAn)
    .then((result) => {
      spinnerService.tatLoading();
      console.log(result);

      renderDanhSachService();
      document.getElementById("myform").reset();
    })
    .catch((error) => {
      spinnerService.tatLoading();

      console.log(error);
    });
};
window.capNhatMonAn = capNhatMonAn;
// setTimeout(() => {
//   console.log("0");
// }, 1000);
// setTimeout(() => {
//   console.log("3");
// }, 100);
// console.log("1");
