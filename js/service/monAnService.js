const BASE_URL = "https://62b3cf60a36f3a973d271421.mockapi.io/mon-an";
export let monAnService = {
  layDanhSachMonAn: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  xoaMonAn: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },
  themMoiMonAn: (monAn) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: monAn,
    });
  },
  layThongTinChiTietMonAn: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "GET",
    });
  },
  capNhatMonAn: (monAn) => {
    return axios({
      url: `${BASE_URL}/${monAn.id}`,
      method: "PUT",
      data: monAn,
    });
  },
};
